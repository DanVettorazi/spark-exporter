import subprocess
import sys, getopt
import os
import shutil
import json
import logging
import csv
from pathlib import Path
import re
import fileinput

sourceJsonFile = ''

def traverse(obj, path=None, callback=None):
	if path is None:
		path = []

	if isinstance(obj, dict):
		value = {k: traverse(v, path + [k], callback)
				 for k, v in obj.items()}
	elif isinstance(obj, list):
		value = [traverse(elem, path + [[]], callback)
				 for elem in obj]
	else:
		value = obj

	if callback is None:  # if a callback is provided, call it to get the new value
		return value
	else:
		return callback(path, value)

def traverse_modify(obj, target_path, action):

	# target_path = to_path(target_path)  # converts 'foo.bar' to ['foo', 'bar']

	# This will get called for every path/value in the structure
	def transformer(path, value):
		if path == target_path:
			print(path)
			return action(value)
		else:
			return value

	return traverse(obj, callback=transformer)


def switchOutputVisibility(json_file_path):

	def callback(path, val):
		if len(path) > 0:
			# if path[-1] == "outputVisibility":

			# 	# outputVisibility : 1 (preview only) -> 2 (capture only)
			# 	if val == 1:
			# 		print (path)
			# 		return 2

			# 	# outputVisibility : 2 (capture only) -> 1 (preview only) 
			# 	elif val == 2:
			# 		print (path)
			# 		return 1
			# 	else:
			# 		return val
					
			# elif path[-1] == "compressionType":
			if path[-1] == "compressionType":
				if val == 2:
					print (path)
					return 0
				else:
					return val
		return val

	with open(json_file_path, 'r+') as json_file:
		data = json.load(json_file)
		new_data= traverse(data, None, callback)
		json_file.seek(0) # rewind to top
		json_file.write(json.dumps(new_data))
	json_file.close()

def main(argv):
	global sourceJsonFile

	try:
		opts, args = getopt.getopt(argv, "hi:", ["sourceJsonFile="])
	except getopt.GetoptError:
		print ('togglePreviewCapture.py -i <sourceJsonFile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('togglePreviewCapture.py -i <sourceJsonFile>')
			sys.exit()
		elif opt in ("-i", "--sourceJsonFile"):
			sourceJsonFile = arg
	
	switchOutputVisibility(sourceJsonFile)


if __name__ == "__main__":
	main(sys.argv[1:])