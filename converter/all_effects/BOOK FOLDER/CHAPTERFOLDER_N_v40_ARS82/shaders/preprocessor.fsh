#ifndef GL_ES
#define precision
#define lowp
#define mediump
#define highp
#endif // !defined(GL_ES)

varying vec2 hdConformedUV;
varying vec2 uv;
uniform sampler2D u_InputImage;
uniform int u_PassIndex;
uniform vec2 u_RenderSize;
uniform float u_Time;

void main() {
  gl_FragColor = texture2D(u_InputImage, uv);
}
