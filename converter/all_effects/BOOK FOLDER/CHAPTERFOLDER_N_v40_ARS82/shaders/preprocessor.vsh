attribute vec3 position;
uniform vec2 u_RenderSize;
varying vec2 hdConformedUV;
varying vec2 uv;

// uniform mat4 u_MMatrix;
// uniform mat4 u_VMatrix;
// uniform mat4 u_PMatrix;
uniform mat4 u_MVPMatrix;
// uniform mat4 u_MVMatrix;
// uniform mat4 u_NormalMatrix;
// varying vec2 v_TexCoords;
uniform float xOffset;
uniform float yOffset;
uniform float scale;

vec4 projectedPosition() {
  return u_MVPMatrix * vec4(position, 1.0);
}


vec2 calculateHdConformedUV(vec2 uv, vec2 u_RenderSize) {
  float longestDim = max(u_RenderSize.x, u_RenderSize.y);
  float ratio = longestDim / 1280.0;
  vec2 hdConformedUV = vec2(0.0);
  if (u_RenderSize.y >= u_RenderSize.x) {
    hdConformedUV.x = uv.x * u_RenderSize.x / (u_RenderSize.x / ratio);
    hdConformedUV.y = uv.y * ratio;
  } else {
    hdConformedUV.x = uv.x * ratio;
    hdConformedUV.y = uv.y * u_RenderSize.y / (u_RenderSize.y / ratio);
  }
  return hdConformedUV;
}

void main() {
  // gl_Position = vec4(position, 1.0);
  vec4 proj = projectedPosition();
  proj.y *= -1.;
  proj.xy *= scale;
  proj.x += xOffset*proj.w;
  proj.y += yOffset*proj.w;
  gl_Position = proj;
  uv = position.xy * 0.5 + 0.5;
  hdConformedUV = calculateHdConformedUV(uv, u_RenderSize);
}
