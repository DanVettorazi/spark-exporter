attribute vec3 a_Position;
attribute vec2 a_TexCoords;
attribute vec4 a_Weight;
attribute vec4 a_Joint;

uniform mat4 u_JointMatrix[20];
uniform mat4 u_MMatrix;
uniform mat4 u_VMatrix;
uniform mat4 u_PMatrix;
uniform mat4 u_MVPMatrix;
uniform mat4 u_MVMatrix;
uniform mat4 u_NormalMatrix;
varying vec2 v_TexCoords;

vec4 projectedPosition() {
  return u_MVPMatrix * vec4(a_Position, 1.0);
}

vec4 projectedPositionJoints() {

  mat4 skinMatrix = a_Weight.x * u_JointMatrix[int(a_Joint.x)] + a_Weight.y * u_JointMatrix[int(a_Joint.y)] + a_Weight.z * u_JointMatrix[int(a_Joint.z)] + a_Weight.w * u_JointMatrix[int(a_Joint.w)];
  return u_MVPMatrix * skinMatrix * vec4(a_Position, 1.0);
}

uniform float facePlaneX;
uniform float facePlaneY;
uniform float facePlaneScaleX;
uniform float portraitScaleMult;

uniform vec2 u_RenderSize;
uniform float focalPlaneDistance;

uniform float deviceInfoWidth;
uniform float deviceInfoHeight;

// 100 / 3
#define LONG_LENGTH_DIVIDER 33.333333333

// 3 / 4
#define LANDSCAPE_SCALER 0.75 

// 4 / 3
#define PORTRAIT_SCALER 1.3333333333

#define X_PORTRAIT_DIVIDER 14.0
#define Y_LANDSCAPE_DIVIDER 18.75

// (3.0 / 10.0) * (3.0 / 4.0) 
#define LANDSCAPE_SCALE_MULTIPLIER 0.225

// (1.0 / 3.0) * (4.0 / 3.0)
#define PORTRAIT_SCALE_MULTIPLIER 0.44444444

// Magic number as rounded avg of ((1.0/3.0*4.0/3)+(1.0/3.0*3.0/4.0))/2
#define PORTRAIT_SCALE_MULTIPLER_AFTER_SWITCH 0.35

// https://stackoverflow.com/questions/46182845/field-of-view-aspect-ratio-view-matrix-from-projection-matrix-hmd-ost-calib
// https://stackoverflow.com/questions/46578529/how-to-compute-the-size-of-the-rectangle-that-is-visible-to-the-camera-at-a-give
// Focal Z is the distance to the focal plane. Defaults in spark to -53.612
vec4 extractFocalPlaneSize(float focalZ){

  const float PI = 3.14159265359;
  float aspect = u_PMatrix[1][1] / u_PMatrix[0][0];
  float fovY = 2.0 * atan( 1.0 / u_PMatrix[1][1]) * (180.0 / PI);

  float t = tan( radians(fovY) / 2.0);
  float h = t * 2.0 * focalZ;
  float w = h * aspect;

  float width = abs(w);
  float height = abs(h);

  return vec4(width, height, aspect, fovY);
}

void main() {
  // Check the first value of the joint matrix to test if we have joints or not.
  // Not sure if this is a good check or not. 
  // We could also just put a manual switch on the material but it would require the tech to flip it on.
  float hasJoints = u_JointMatrix[0][0][0];

  vec4 proj = vec4(0.0);

  if(hasJoints == 0.0){
    proj = projectedPosition();
  } else {
    proj = projectedPositionJoints();
  }

  float xOff = 0.0;

  float yOff = 0.0;
  float scaleOff = 1.0;

  vec4 focalPlaneData = extractFocalPlaneSize(focalPlaneDistance);
  float focalPlaneWidth = focalPlaneData.x;
  float focalPlaneHeight = focalPlaneData.y;
  float aspect = focalPlaneData.z;

  // Not using isLandscape here but not 100% sure that these update when you switch orientations
  if(focalPlaneWidth > focalPlaneHeight){
    // Landscape
  // if(u_RenderSize.x > u_RenderSize.y){
    xOff = (facePlaneX / LONG_LENGTH_DIVIDER) * LANDSCAPE_SCALER;
    yOff = (facePlaneY / Y_LANDSCAPE_DIVIDER) * LANDSCAPE_SCALER;
    scaleOff = facePlaneScaleX * LANDSCAPE_SCALE_MULTIPLIER;
  } else {
    // Portrait
    xOff = facePlaneX / X_PORTRAIT_DIVIDER;
    yOff = (facePlaneY / LONG_LENGTH_DIVIDER) * PORTRAIT_SCALER;
    
    // Take advantage of the fact that deviceInfo from patch doesn't update itself on orientation changes
    if(deviceInfoWidth > deviceInfoHeight){
      // We started in landscape so use this value for portrait instead
      scaleOff = facePlaneScaleX * PORTRAIT_SCALE_MULTIPLER_AFTER_SWITCH;
    } else {
      // We started in portrait so all is well
      scaleOff = facePlaneScaleX * PORTRAIT_SCALE_MULTIPLIER;
    }
  }

  proj.xy *= scaleOff;
  proj.x += xOff * proj.w;
  proj.y += yOff * proj.w;

  gl_Position = proj;
  v_TexCoords = a_TexCoords;

}