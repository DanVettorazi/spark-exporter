//#region Requirements
const S = require('Scene');
const R = require('Reactive');
const A = require('Animation');
const D = require('Diagnostics');
const F = require('FaceTracking');
const M = require('Materials');
const T = require('Textures');
const P = require('Patches');
const CI = require('CameraInfo'); // Also turn on in Capabilities
// const DM = require('DeviceMotion');    // Also turn on in Capabilities
// const TG = require('TouchGestures');   // Also turn on in Capabilities
const Time = require('Time');

// Ingest objects from scene
var device = S.root.child('Device');
var camera = device.child('Camera');
var focal = camera.child('Focal Distance');
var ft0 = focal.child('facetracker0');
var face0 = F.face(0);
// var faceMesh = S.root.find('facemesh0');
//#endregion
S.root.find("canvas").hidden = true;

// Add additional materials to this array if they need to track to the face.
const faceTrackedMaterials = ['otto_makeup', 'otto_eyeRing', 'otto_ear_L', 'otto_ear_R'];

//  ----------------------------------------------------------------------
//
//     Portrait/Landscape Detection and camera null to face link
//
//  ----------------------------------------------------------------------

// #region Portal Landscape / Portrait Pip & AR Mask Scaling
// =========================================================
const facePlane = S.root.find('facePlane'); // Plane object that camera texture (from Patches) is applied to.
// const faceTrackedMaterials = []; // make sure to uncomment this if you have not yet defined your face tracked materials
// Detect Landscape/portrait
const focalWidth = camera.focalPlane.width;
const focalHeight = camera.focalPlane.height;
const isLandscape = focalWidth.gt(focalHeight);
P.setBooleanValue('isLandscape', isLandscape);
P.setScalarValue('screenHeight', focalHeight);
P.setScalarValue('screenWidth', focalWidth);

// Bind facePlane streams and set shader uniforms for all materials
for (var j = 0; j < faceTrackedMaterials.length; j++) {
  M.get(faceTrackedMaterials[j]).setFloatParameter('facePlaneX', facePlane.worldTransform.x);
  M.get(faceTrackedMaterials[j]).setFloatParameter('facePlaneY', facePlane.worldTransform.y);
  M.get(faceTrackedMaterials[j]).setFloatParameter(
    'facePlaneScaleX',
    facePlane.worldTransform.scaleX
  );
  M.get(faceTrackedMaterials[j]).setFloatParameter(
    'focalPlaneDistance',
    camera.focalPlane.distance
  );
  M.get(faceTrackedMaterials[j]).setFloatParameter('deviceInfoWidth', CI.previewSize.width);
  M.get(faceTrackedMaterials[j]).setFloatParameter('deviceInfoHeight', CI.previewSize.height);
}
// #endregion

//  ----------------------------------------------------------------------
//
//     Utility Functions
//
//  ----------------------------------------------------------------------

function getUFrustum(x, z) {
  const fx = R.val(-0.26648)
    .mul(z)
    .sum(14.1929);
  return x
    .sub(fx)
    .div(fx.mul(2))
    .sum(1);
}

function remap_signal_range(signal, low1, high1, low2, high2) {
  return R.val(low2).add(
    R.sub(high2, low2)
      .mul(R.sub(signal, low1))
      .div(R.sub(high1, low1))
  );
}

//sprite importer

// sheet 0

(() => {
  // name of sprite sheet (not including suffixes like .png or .jpg)
  const spriteSheetName = 'Otto_Book_02_Beach_static_CHAPTER390_0';
  // array of all the materials in the scene (spritesheet will be generated with images in the *same order*)
  const materialNameArray = [
    'card_0450_purpFish_sh',
    'card_0090_rightArm_sh',
    'card_0370_catface_sh',
    'card_0250_sheepRightEar_sh',
    'card_0200_sheepLarm_sh',
    'card_0320_catarmL_sh',
    'card_0150_headFace_sh',
    'card_0120_leftEar_sh',
    'card_0360_catearL_sh',
    'card_0080_rightLeg_sh',
    'card_0140_torso_sh',
    'card_0290_catlegL_sh',
    'card_0350_catearR_sh',
    'card_0170_ottoBall_sh',
    'card_0460_yellowFish_sh',
    'card_0100_leftArm_sh',
    'card_0070_leftLeg_sh',
    'card_0280_sheepBall_sh',
    'card_0300_catlegR_sh',
    'card_0190_sheepLleg_sh',
    'card_0440_greenfish_sh',
    'card_0220_sheepTail_sh',
    'card_0110_rightEar_sh',
    'card_0180_sheepRleg_sh',
    'card_0260_sheepLeftEar_sh',
    'card_0210_sheepRarm_sh',
    'card_0340_catdress_sh',
    'card_0390_cateyes_sh',
    'card_0430_redFish_sh',
    'card_0130_tail_sh',
    'card_0270_sheepHead_sh',
    'card_0240_sheepHair_sh',
    'card_0230_sheepTorso_sh',
    'card_0310_catarmR_sh'
  ];
  // "subTexture" string is always the same
  const subTextureString = '_subTexture';
  for (let i = 0; i < materialNameArray.length; i++) {
    let mat = M.get(materialNameArray[i]);
    let subTextureNumber = i == 0 ? '' : i - 1;
    let subTexture = T.get(spriteSheetName + subTextureString + subTextureNumber.toString());
    mat.diffuse = subTexture;
  }
})();

// const ChapterBug = S.root.find('ChapterBug');
// let books = ["PASCH", "LLMM", "LLRP", "PABEA", "PABED", "PAPAR", "LLBG", "LLTS"]
// ChapterBug.setStringInput('Book', books[3]);
// ChapterBug.setScalarInput('Chapter', 39);
// ChapterBug.setScalarInput('Version', 40);
// ChapterBug.setBooleanInput('HalfChapter', false);

// production-scene-scaler-begin - auto generated - do not commit
const _productionScene = require("Scene");
const _productionDevice = _productionScene.root.child("Device");
const _productionCamera = _productionDevice.child("Camera");

const _productionSceneScalerNull = _productionScene.root.find("sceneScaler");
const _productionScaleUp = _productionCamera.focalPlane.width.gt(_productionCamera.focalPlane.height).ifThenElse(1.34, 1.0);
_productionSceneScalerNull.transform.scaleX = _productionSceneScalerNull.transform.scaleY = _productionScaleUp;
// production-scene-scaler-end
