#!/usr/bin/env python
# (c) Facebook, Inc. and its affiliates. Confidential and proprietary.

from __future__ import absolute_import, division, print_function, unicode_literals

import argparse
import io
import json
import os
import re
import sys
import uuid
from collections import namedtuple
from copy import deepcopy
from io import BytesIO
from zipfile import ZIP_DEFLATED, ZipFile


MIN_PROJECT_VERSION = 49

COMPRESSION_LEVEL = 9
ZIP_FILE_KWARGS = {
    "compression": ZIP_DEFLATED,
    # "compresslevel": COMPRESSION_LEVEL,
}

COLOR_LUT_PRESET_FRAGMENT_SHADER = """
varying vec2 uv;
uniform sampler2D inputImage;
uniform sampler2D colorlut;

#ifdef APPLY_COLOR_LUT
// apply a color LUT to the input color
vec4 colorLUT(vec4 color, sampler2D lut) {
  // this should come from a real uniform from the engine
  // hard-coded here for now
  const vec4 uLutSize = vec4(colorlut_width, colorlut_height, 1.0/colorlut_width, 1.0/colorlut_height);

  vec3 scaledColor = color.xyz * (uLutSize.y - 1.0);
  float bFrac = fract(scaledColor.z);

  // offset by 0.5 pixel and fit within range [0.5, width-0.5]
  // to prevent bilinear filtering with adjacent colors
  vec2 texc = (0.5 + scaledColor.xy) * uLutSize.zw;

  // offset by the blue slice
  texc.x += (scaledColor.z - bFrac) * uLutSize.w;

  // sample the 2 adjacent blue slices
  vec3 b0 = texture2D(lut, texc).xyz;
  vec3 b1 = texture2D(lut, vec2(texc.x + uLutSize.w, texc.y)).xyz;

  // blend between the 2 adjacent blue slices
  color.xyz = mix(b0, b1, bFrac);

  return color;
}
#endif

void main() {
    vec4 outputPix = texture2D(inputImage, uv);
#ifdef APPLY_COLOR_LUT
    outputPix = colorLUT(outputPix, colorlut);
#endif
    gl_FragColor = outputPix;
}
"""
COLOR_LUT_PRESET_FRAGMENT_SHADER = COLOR_LUT_PRESET_FRAGMENT_SHADER[1:]

COLOR_LUT_PRESET_VERTEX_SHADER = """
attribute vec3 position;
uniform vec2 uRenderSize;
varying vec2 hdConformedUV;
varying vec2 uv;

vec2 calculateHdConformedUV(vec2 uv, vec2 uRenderSize) {
  float longestDim = max(uRenderSize.x, uRenderSize.y);
  float ratio = longestDim / 1280.0;
  vec2 hdConformedUV = vec2(0.0);
  if (uRenderSize.y >= uRenderSize.x) {
    hdConformedUV.x = uv.x * uRenderSize.x / (uRenderSize.x / ratio);
    hdConformedUV.y = uv.y * ratio;
  } else {
    hdConformedUV.x = uv.x * ratio;
    hdConformedUV.y = uv.y * uRenderSize.y / (uRenderSize.y / ratio);
  }
  return hdConformedUV;
}

void main() {
  gl_Position = vec4(position, 1.0);
  uv = position.xy * 0.5 + 0.5;
  hdConformedUV = calculateHdConformedUV(uv, uRenderSize);
}
"""
COLOR_LUT_PRESET_VERTEX_SHADER = COLOR_LUT_PRESET_VERTEX_SHADER[1:]

GLSL_FRAGMENT_SHADER_HEADER = """
#ifndef GL_ES
#define precision
#define lowp
#define mediump
#define highp
#endif // !defined(GL_ES)

"""
GLSL_FRAGMENT_SHADER_HEADER = GLSL_FRAGMENT_SHADER_HEADER[1:]


class ProcessorType(int):
    PRE = 0
    FIRST = PRE

    POST = 1
    LAST = POST

    @staticmethod
    def all():
        for i in range(ProcessorType.FIRST, ProcessorType.LAST + 1):
            yield ProcessorType(i)

    @staticmethod
    def count():
        return ProcessorType.LAST + 1

    @property
    def name(self):
        return ["preprocessor", "postprocessor"][int(self)]

    @property
    def pretty_name(self):
        return ["preProcessor", "postProcessor"][int(self)]


SCRIPT_GETTER_NAMES = ProcessorType.count() * [None]
SCRIPT_GETTER_NAMES[ProcessorType.PRE] = "preProcessor"
SCRIPT_GETTER_NAMES[ProcessorType.POST] = "postProcessor"

SCRIPT_FUNCTION_NAMES = [
    "setFloatParameter",
    "floatParameter",
    "setBoolParameter",
    "boolParameter",
]

MAX_PROCESSOR_TEXTURE_COUNT = 7

RenderPassTarget = namedtuple("RenderPassTarget", ["name", "size", "width", "height"])


class Processor(object):
    def __init__(self):
        self.model = None
        self.material_identifiers = []
        self.material_names = []
        self.shader_identifier = None
        self.vertex_shader_path = None
        self.vertex_shader_model = None
        self.fragment_shader_path = None
        self.fragment_shader_model = None
        self.shader_parameters = None
        self.camera_identifier = None
        self.device_identifier = None
        self.layer_identifier = None
        self.render_pass_targets = []
        self.render_pass_texture_identifiers = []
        self.texture_identifiers = MAX_PROCESSOR_TEXTURE_COUNT * [None]
        self.texture_names = MAX_PROCESSOR_TEXTURE_COUNT * [None]


def make_random_identifier(model_name):
    id_ = str(uuid.uuid4()).upper()
    return "{}:{}".format(model_name, id_)


class ProjectVersionException(Exception):
    pass


class ProjectMigrator(object):
    def __init__(self):
        self.processors = ProcessorType.count() * [None]
        self.script_paths = []
        self.camera_texture_identifier = None
        self.camera_null_object_identifier = None
        self.camera_render_pass_texture_identifier = None
        self.scene_render_pass_texture_identifier = None
        self.final_render_pass_identifier = None

    @property
    def shader_paths(self):
        paths = []

        for processor_type in ProcessorType.all():
            index = int(processor_type)
            processor = self.processors[index]
            if not processor:
                continue
            paths.append(processor.vertex_shader_path)
            paths.append(processor.fragment_shader_path)

        return paths

    def migrate_manifest(self, input_manifest):
        print("Migrating manifest")

        output_manifest = deepcopy(input_manifest)
        output_manifest["shaderPreProcessor"] = False
        output_manifest["shaderPostProcessor"] = False

        # Image anti-aliasing built-in processor
        capability_name = "effectUsesImageAntiAliasing"
        if capability_name in output_manifest:
            if output_manifest[capability_name]:
                print("Removing image anti-aliasing")

            output_manifest[capability_name] = False
            # Reset to default values
            output_manifest["effectImageAntiAliasingEdgeSharpness"] = 0.5
            output_manifest["effectImageAntiAliasingEdgeThreshold"] = 0.5
            output_manifest["effectImageAntiAliasingTechnique"] = 0
            # Remove manually enabled capability
            if "manuallyEnabledItemsIds" in output_manifest:
                manually_enabled_capabilities = output_manifest[
                    "manuallyEnabledItemsIds"
                ]
                if capability_name in manually_enabled_capabilities:
                    manually_enabled_capabilities.remove(capability_name)

        # RGB Camera texture
        capability_name = "effectUsesRGBCameraTexture"
        if capability_name in output_manifest:
            if output_manifest[capability_name]:
                print("Removing RGB camera texture")

            output_manifest[capability_name] = False
            # Remove manually enabled capability
            if "manuallyEnabledItemsIds" in output_manifest:
                manually_enabled_capabilities = output_manifest[
                    "manuallyEnabledItemsIds"
                ]
                if capability_name in manually_enabled_capabilities:
                    manually_enabled_capabilities.remove(capability_name)

        # Enable custom material
        print("Enabling custom material")

        output_manifest["effectUsesCustomMaterial"] = True

        # Enable custom render pipeline
        print("Enabling custom render pipeline")

        output_manifest["effectUsesCustomPipeline"] = True

        if "manuallyEnabledItemsIds" not in output_manifest:
            output_manifest["manuallyEnabledItemsIds"] = []

        output_manifest["manuallyEnabledItemsIds"].append("effectUsesCustomPipeline")

        return output_manifest

    def migrate_shader(self, input_shader, shader_name):
        print('Migrating shader: "{}"'.format(shader_name))

        str_ = input_shader.decode("utf-8")
        if shader_name.endswith(".fsh"):  # Fragment Shader
            str_ = GLSL_FRAGMENT_SHADER_HEADER + str_
        # Parameters
        str_ = re.sub(r"\buTime\b", "u_Time", str_)
        str_ = re.sub(r"\buRenderSize\b", "u_RenderSize", str_)
        str_ = re.sub(r"\bpassIndex\b", "u_PassIndex", str_)
        # Textures (and size)
        str_ = re.sub(r"\binputImage\b", "u_InputImage", str_)
        str_ = re.sub(r"\buInputImageSize\b", "u_InputImageSize", str_)

        return str_.encode("utf-8")

    def get_shader_config_path(self, processor_type):
        index = int(processor_type)
        if self.processors[index] is None:
            return None

        filename = processor_type.name + ".json"
        return "shaders/{}".format(filename)

    def migrate_shader_config(self, processor_type):
        index = int(processor_type)
        assert self.processors[index] is not None

        config_path = self.get_shader_config_path(processor_type)
        print('Generating shader config: "{}"'.format(config_path))

        processor = self.processors[index]
        shader_parameters = processor.shader_parameters

        config_dict = {}
        config_dict["textures"] = []
        config_dict["parameters"] = []

        # Shader textures
        config_parameters = config_dict["textures"]
        config_parameter = {}
        name = "u_InputImage"
        config_parameter[name] = {}

        config_parameter_values = config_parameter[name]
        config_parameter_values["displayName"] = "Input Image"
        config_parameter_values["hidden"] = False

        config_parameters.append(config_parameter)

        for render_pass_target in processor.render_pass_targets:
            name = render_pass_target.name
            if name == "last":
                continue

            config_parameter = {}
            config_parameter[name] = {}

            config_parameter_values = config_parameter[name]
            config_parameter_values["displayName"] = name
            config_parameter_values["hidden"] = False

            config_parameters.append(config_parameter)

        for i in range(MAX_PROCESSOR_TEXTURE_COUNT):
            texture_identifier = processor.texture_identifiers[i]
            if texture_identifier is None:
                continue

            name = processor.texture_names[i]

            config_parameter = {}
            config_parameter[name] = {}

            config_parameter_values = config_parameter[name]
            config_parameter_values["displayName"] = name
            config_parameter_values["hidden"] = False

            config_parameters.append(config_parameter)

        # Shader parameters
        config_parameters = config_dict["parameters"]

        if "children" in shader_parameters:
            for shader_parameter in shader_parameters["children"]:
                keys = ("name", "displayName", "type")
                (name, display_name, type_) = (shader_parameter[key] for key in keys)

                value = shader_parameter.get("values", None)
                value = shader_parameter.get("value", value)

                min_ = shader_parameter.get("min", None)
                max_ = shader_parameter.get("max", None)

                # hidden = shader_parameter.get("hidden", False)

                type_ = {
                    0: "bool",
                    1: "int",
                    2: "float",
                    3: "color",
                    4: None,  # 'label'
                }[type_]

                assert type_ is not None

                config_parameter = {}
                config_parameter[name] = {}

                config_parameter_values = config_parameter[name]
                config_parameter_values["displayName"] = display_name
                config_parameter_values["value"] = value
                config_parameter_values["type"] = type_
                if min_ is not None:
                    config_parameter_values["min"] = min_
                if max_ is not None:
                    config_parameter_values["max"] = max_
                config_parameter_values["hidden"] = False  # hidden (ignored in Classic)

                config_parameters.append(config_parameter)

        # Create a dedicated uniform for the pass index
        config_parameter = {}
        config_parameter["u_PassIndex"] = {}

        config_parameter_values = config_parameter["u_PassIndex"]
        config_parameter_values["displayName"] = "Render Pass Index"
        config_parameter_values["value"] = 0
        config_parameter_values["type"] = "int"
        config_parameter_values["min"] = 0
        config_parameter_values["max"] = 8
        config_parameter_values["hidden"] = False

        config_parameters.append(config_parameter)

        return json.dumps(config_dict, sort_keys=True, indent=2).encode("utf-8")

    def process_vertex_shader(self, input_shader, processor_type):
        shader_path = input_shader["assetLocator"]

        print('Found vertex shader: "{}"'.format(shader_path))
        index = int(processor_type)
        processor = self.processors[index]
        processor.vertex_shader_path = shader_path
        processor.vertex_shader_model = deepcopy(input_shader)

    def process_fragment_shader(self, input_shader, processor_type):
        shader_path = input_shader["assetLocator"]

        print('Found fragment shader: "{}"'.format(shader_path))
        index = int(processor_type)
        processor = self.processors[index]
        processor.fragment_shader_path = shader_path
        processor.fragment_shader_model = deepcopy(input_shader)

    def process_shader_parameters(self, input_shader_parameters, processor_type):
        index = int(processor_type)
        self.processors[index].shader_parameters = deepcopy(input_shader_parameters)

    def migrate_processor(self, input_processor, processor_type):
        if processor_type == ProcessorType.PRE:
            print("Migrating pre-processor")
        elif processor_type == ProcessorType.POST:
            print("Migrating post-processor")
        else:
            raise AssertionError()

        index = int(processor_type)
        assert self.processors[index] is None
        self.processors[index] = Processor()
        processor = self.processors[index]

        pass_buffers = None
        pass_targets = None

        if "children" in input_processor:
            for item in input_processor["children"]:
                if item["modelName"] == "vertex_shader_model":
                    self.process_vertex_shader(item, processor_type)
                elif item["modelName"] == "fragment_shader_model":
                    self.process_fragment_shader(item, processor_type)
                elif item["modelName"] == "shader_parameters_model":
                    self.process_shader_parameters(item, processor_type)
                elif item["modelName"] == "passBuffers":
                    pass_buffers = item
                elif item["modelName"] == "passTargets":
                    pass_targets = item

        processor.model = deepcopy(input_processor)

        render_pass_target_map = {}

        if "children" in pass_buffers:
            for pass_buffer in pass_buffers["children"]:
                name = pass_buffer["name"]
                size = pass_buffer["size"]
                width = pass_buffer.get("width", 0)
                height = pass_buffer.get("height", 0)
                render_pass_target_map[name] = RenderPassTarget(
                    name, size, width, height
                )

        render_pass_targets = []

        if "children" in pass_targets:
            for pass_target in pass_targets["children"]:
                name = pass_target["name"]
                if name not in render_pass_target_map:
                    render_pass_target_map[name] = RenderPassTarget(name, 1, 0, 0)
                render_pass_targets.append(render_pass_target_map[name])

        render_pass_targets.append(RenderPassTarget("last", 1, 0, 0))

        for i in range(MAX_PROCESSOR_TEXTURE_COUNT):
            index = i + 1
            key = "isTexture{}IdentifierEnabled".format(index)
            if not input_processor[key]:
                continue
            key = "texture{}".format(index)
            if key not in input_processor:
                continue
            texture_identifier = input_processor[key]
            processor.texture_identifiers[i] = texture_identifier

        processor.render_pass_targets = render_pass_targets
        return None

    def migrate_script(self, input_script, script_name):
        print('Migrating script: "{}"'.format(script_name))
        str_ = input_script.decode("utf-8")

        for processor_type in ProcessorType.all():
            index = int(processor_type)
            processor = self.processors[index]
            if processor is None:
                continue
            script_getter_name = SCRIPT_GETTER_NAMES[processor_type]
            material_name = processor.material_names[0]  # FIXME
            # Convert "M.preProcessor.setFloatParameter(...);"
            for script_function_name in SCRIPT_FUNCTION_NAMES:
                str_ = re.sub(
                    r"\b{}\.{}\b".format(script_getter_name, script_function_name),
                    'get("{}").{}'.format(material_name, script_function_name),
                    str_,
                )
            # Convert "var pre = M.preProcessor;"
            str_ = re.sub(
                r"\b(M|Materials)\.{}\b".format(script_getter_name),
                r'\1.get("{}")'.format(material_name),
                str_,
            )

        return str_.encode("utf-8")

    def migrate_materials(self, input_materials):
        print("Migrating materials")

        return deepcopy(input_materials)

    def migrate_render_passes(self, input_render_passes):
        print("Migrating render passes")

        return deepcopy(input_render_passes)

    def migrate_shaders(self, input_shaders):
        print("Migrating shaders")

        return deepcopy(input_shaders)

    def migrate_textures(self, input_textures):
        print("Migrating textures")

        return deepcopy(input_textures)

    def create_processor_materials(self, materials):
        print("Migrating processor materials")

        if "children" not in materials:
            materials["children"] = []

        for processor_type in ProcessorType.all():
            index = int(processor_type)
            processor = self.processors[index]
            if processor is None:
                continue

            assert processor.shader_identifier is not None

            processor_name = processor_type.pretty_name

            for i, render_pass_target in enumerate(processor.render_pass_targets):
                name = render_pass_target.name

                render_pass_material_name = processor_name + "Material_" + name
                render_pass_material_identifier = make_random_identifier(
                    "custom_material_model"
                )

                processor.material_identifiers.append(render_pass_material_identifier)
                processor.material_names.append(render_pass_material_name)

                material_model = {}
                material_model["identifier"] = render_pass_material_identifier
                material_model["modelName"] = "custom_material_model"
                material_model["name"] = render_pass_material_name

                material_model["alphaTestEnabled"] = False
                material_model["alphaTestThreshold"] = 0
                material_model["blendMode"] = 5  # Alpha
                material_model["cullMode"] = 0  # Back
                material_model["diffuseTextureTransform"] = {}
                material_model["diffuseTextureTransform"]["offsetU"] = 0
                material_model["diffuseTextureTransform"]["offsetV"] = 0
                material_model["diffuseTextureTransform"]["scaleU"] = 1
                material_model["diffuseTextureTransform"]["scaleV"] = 1
                material_model["doubleSided"] = False
                material_model["importIssues"] = {}
                material_model["isDiffuseTextureIdentifierEnabled"] = False
                material_model["readDepth"] = True
                material_model["shaderIdentifier"] = processor.shader_identifier
                material_model["transparency"] = 1
                material_model["writeDepth"] = True
                material_model["writesToColorBuffer"] = 0xF  # Enable RGBA writes

                material_model["children"] = []
                children = material_model["children"]

                # Shader parameters
                material_parameters_model = {}
                model_name = "custom_material_parameters_model"
                material_parameters_model["identifier"] = make_random_identifier(
                    model_name
                )
                material_parameters_model["modelName"] = model_name
                material_parameters_model["children"] = []

                if "children" in processor.shader_parameters:
                    for shader_parameter in processor.shader_parameters["children"]:
                        keys = ("name", "type")
                        (name, type_) = (shader_parameter[key] for key in keys)

                        value = shader_parameter.get("values", None)
                        value = shader_parameter.get("value", value)

                        material_parameter_model = {}
                        model_name = "custom_material_parameter_model"
                        material_parameter_model["identifier"] = make_random_identifier(
                            model_name
                        )
                        material_parameter_model["modelName"] = model_name
                        material_parameter_model["name"] = name
                        material_parameter_model["type"] = type_
                        material_parameter_model["value"] = value

                        material_parameters_model["children"].append(
                            material_parameter_model
                        )

                # Shader parameters: add render pass index
                material_parameter_model = {}
                model_name = "custom_material_parameter_model"
                material_parameter_model["identifier"] = make_random_identifier(
                    model_name
                )
                material_parameter_model["modelName"] = model_name
                material_parameter_model["name"] = "u_PassIndex"
                material_parameter_model["type"] = 1  # int
                material_parameter_model["value"] = i

                material_parameters_model["children"].append(material_parameter_model)

                children.append(material_parameters_model)

                # Shader textures
                material_textures_model = {}
                model_name = "custom_material_texture_parameters_model"
                material_textures_model["identifier"] = make_random_identifier(
                    model_name
                )
                material_textures_model["modelName"] = model_name
                material_textures_model["children"] = []

                # Shader textures: add all render pass inputs
                render_pass_texture_identifiers = (
                    processor.render_pass_texture_identifiers
                )

                material_texture_model = {}
                model_name = "custom_material_texture_parameter_model"
                material_texture_model["identifier"] = make_random_identifier(
                    model_name
                )
                material_texture_model["modelName"] = model_name
                material_texture_model["name"] = "u_InputImage"
                material_texture_model["isTextureIdenfierEnabled"] = True
                if processor_type == ProcessorType.POST:
                    texture_identifier = self.scene_render_pass_texture_identifier
                else:
                    texture_identifier = self.camera_render_pass_texture_identifier
                material_texture_model["textureIdentifier"] = texture_identifier

                material_textures_model["children"].append(material_texture_model)

                for j, render_pass_target in enumerate(processor.render_pass_targets):
                    name = render_pass_target.name
                    if name == "last":
                        continue

                    material_texture_model = {}
                    model_name = "custom_material_texture_parameter_model"
                    material_texture_model["identifier"] = make_random_identifier(
                        model_name
                    )
                    material_texture_model["modelName"] = model_name
                    material_texture_model["name"] = name
                    material_texture_model["isTextureIdenfierEnabled"] = False
                    material_texture_model["textureIdentifier"] = None

                    if j < i:
                        material_texture_model["isTextureIdenfierEnabled"] = True
                        material_texture_model[
                            "textureIdentifier"
                        ] = render_pass_texture_identifiers[j]

                    material_textures_model["children"].append(material_texture_model)

                # Shader textures: add auxiliary textures
                for j in range(MAX_PROCESSOR_TEXTURE_COUNT):
                    texture_identifier = processor.texture_identifiers[j]
                    if texture_identifier is None:
                        continue

                    texture_name = processor.texture_names[j]
                    material_texture_model = {}
                    model_name = "custom_material_texture_parameter_model"
                    material_texture_model["identifier"] = make_random_identifier(
                        model_name
                    )
                    material_texture_model["modelName"] = model_name
                    material_texture_model["name"] = texture_name
                    material_texture_model["isTextureIdenfierEnabled"] = True
                    material_texture_model["textureIdentifier"] = texture_identifier
                    material_textures_model["children"].append(material_texture_model)

                children.append(material_textures_model)

                materials["children"].append(material_model)

    def create_scene_object(self, name, layer_identifier=None):
        scene_object_model = {}
        model_name = "sceneObject"
        scene_object_model["identifier"] = make_random_identifier(model_name)
        scene_object_model["modelName"] = model_name
        scene_object_model["name"] = name

        scene_object_model["hidden"] = False
        scene_object_model["layerIdentifier"] = layer_identifier
        scene_object_model["cameraVisibility"] = 3
        scene_object_model["outputVisibility"] = 3
        scene_object_model["animationMask"] = {
            "position": False,
            "rotation": False,
            "scale": False,
        }
        scene_object_model["transform"] = {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0,
            "rx": 0.0,
            "ry": 0.0,
            "rz": 0.0,
            "sx": 1.0,
            "sy": 1.0,
            "sz": 1.0,
        }

        return scene_object_model

    def create_null_object(self, scene):
        camera_identifier = self.camera_identifier
        layer_identifier = self.layer_identifier

        null_object = self.create_scene_object("cameraNullObject", layer_identifier)
        self.camera_null_object_identifier = null_object["identifier"]

        # Find camera node
        S = [scene]

        while S:
            item = S[-1]

            if (
                item["modelName"] == "camera"
                and item["identifier"] == camera_identifier
            ):
                if "children" not in item:
                    item["children"] = []

                item["children"].append(null_object)
                break

            if "children" in item:
                S = item["children"] + S

            S = S[:-1]

    def create_render_pass_base(self, name):
        render_pass_model = {}
        model_name = "render_pass_base_model"
        render_pass_model["identifier"] = make_random_identifier(model_name)
        render_pass_model["modelName"] = model_name
        render_pass_model["name"] = name

        render_pass_model["sizeMode"] = 1  # SCREEN
        render_pass_model["width"] = 1
        render_pass_model["height"] = 1

        render_pass_model["backgroundIdentifier"] = None
        render_pass_model["format"] = 3  # RGBA
        render_pass_model["dataType"] = 0  # UNSIGNED_BYTE

        render_pass_model["textureOutputIdentifier"] = None
        render_pass_model["isBackgroundIdentifierEnabled"] = True
        render_pass_model["isTextureOutputIdentifierEnabled"] = True

        return render_pass_model

    def create_processor_render_pass(
        self,
        render_pass_target,
        name_format,
        render_pass_texture_identifier,
        material_identifier,
    ):
        name, size, width, height = render_pass_target
        render_pass_model = self.create_render_pass_base(name_format.format(name))
        render_pass_model["modelName"] = "processor_render_pass_model"
        render_pass_model["backgroundIdentifier"] = None
        render_pass_model["textureOutputIdentifier"] = render_pass_texture_identifier
        if size and (not width or not height):
            render_pass_model["sizingMode"] = 1  # SCREEN
            render_pass_model["width"] = size
            render_pass_model["height"] = size
        else:
            render_pass_model["sizingMode"] = 0  # FIXED
            render_pass_model["width"] = width
            render_pass_model["height"] = height
        render_pass_model["materialIdentifier"] = material_identifier
        render_pass_model["isMaterialIdentifierEnabled"] = True

        return render_pass_model

    def create_render_pass_texture(self, name):
        texture_model = {}
        model_name = "render_pass_texture_model"
        texture_model["identifier"] = make_random_identifier(model_name)
        texture_model["modelName"] = model_name
        texture_model["name"] = name

        return texture_model

    def create_processor_render_pass_textures(self, textures):
        print("Creating processor render pass textures")

        if "children" not in textures:
            textures["children"] = []

        for child in textures["children"]:
            if child["modelName"] == "cameraTexture":
                self.camera_texture_identifier = child["identifier"]

        if self.camera_texture_identifier is None:
            assert self.camera_identifier is not None

            camera_texture_model = {}
            model_name = "cameraTexture"
            camera_texture_identifier = make_random_identifier(model_name)
            camera_texture_model["identifier"] = camera_texture_identifier
            camera_texture_model["modelName"] = model_name
            camera_texture_model["name"] = "cameraTexture"
            camera_texture_model["sourceSceneObjectIdentifier"] = self.camera_identifier

            textures["children"].append(camera_texture_model)

            self.camera_texture_identifier = camera_texture_identifier

        processor_type = ProcessorType.PRE
        index = int(processor_type)
        pre_processor = self.processors[index]

        processor_type = ProcessorType.POST
        index = int(processor_type)
        post_processor = self.processors[index]

        # Create (camera) Scene Render Pass Texture
        texture_model = self.create_render_pass_texture("cameraRenderPassTexture")
        self.camera_render_pass_texture_identifier = texture_model["identifier"]
        textures["children"].append(texture_model)

        # Create (Pre) Processor Render Pass Texture
        if pre_processor is not None:
            for render_pass_target in pre_processor.render_pass_targets:
                name = render_pass_target.name
                texture_model = self.create_render_pass_texture(
                    "preProcessorRenderPassTexture_{}".format(name)
                )
                pre_processor_render_pass_texture_identifier = texture_model[
                    "identifier"
                ]
                textures["children"].append(texture_model)

                pre_processor.render_pass_texture_identifiers.append(
                    pre_processor_render_pass_texture_identifier
                )

        # Create (main) Scene Render Pass Texture
        texture_model = self.create_render_pass_texture("sceneRenderPassTexture")
        self.scene_render_pass_texture_identifier = texture_model["identifier"]
        textures["children"].append(texture_model)

        # Create (Post) Processor Render Pass Texture
        if post_processor is not None:
            for render_pass_target in post_processor.render_pass_targets:
                name = render_pass_target.name
                texture_model = self.create_render_pass_texture(
                    "postProcessorRenderPassTexture_{}".format(name)
                )
                post_processor_render_pass_texture_identifier = texture_model[
                    "identifier"
                ]
                textures["children"].append(texture_model)

                post_processor.render_pass_texture_identifiers.append(
                    post_processor_render_pass_texture_identifier
                )

    def create_processor_render_passes(self, render_passes, textures):
        print("Creating processor render passes")

        if "children" not in render_passes:
            render_passes["children"] = []

        if "children" not in textures:
            textures["children"] = []

        assert self.device_identifier is not None

        processor_type = ProcessorType.PRE
        index = int(processor_type)
        pre_processor = self.processors[index]

        processor_type = ProcessorType.POST
        index = int(processor_type)
        post_processor = self.processors[index]

        # Create (camera) Scene Render Pass
        render_pass_model = self.create_render_pass_base("cameraRenderPass")
        render_pass_model["modelName"] = "scene_render_pass_model"
        camera_render_pass_identifier = render_pass_model["identifier"]
        render_pass_model["backgroundIdentifier"] = self.camera_texture_identifier
        render_pass_model[
            "textureOutputIdentifier"
        ] = self.camera_render_pass_texture_identifier
        render_pass_model["sceneNodeIdentifier"] = self.camera_null_object_identifier
        render_passes["children"].append(render_pass_model)

        last_render_pass_texture_identifier = self.camera_render_pass_texture_identifier
        last_render_pass_identifier = camera_render_pass_identifier

        if pre_processor is not None:
            for i, render_pass_target in enumerate(pre_processor.render_pass_targets):
                pre_processor_render_pass_texture_identifier = pre_processor.render_pass_texture_identifiers[
                    i
                ]

                # Create (Pre) Processor Render Pass
                render_pass_model = self.create_processor_render_pass(
                    render_pass_target,
                    "preProcessorRenderPass_{}",
                    pre_processor_render_pass_texture_identifier,
                    pre_processor.material_identifiers[i],
                )
                render_passes["children"].append(render_pass_model)

                last_render_pass_texture_identifier = (
                    pre_processor_render_pass_texture_identifier
                )
                last_render_pass_identifier = render_pass_model["identifier"]

        # Create (main) Scene Render Pass
        render_pass_model = self.create_render_pass_base("sceneRenderPass")
        render_pass_model["modelName"] = "scene_render_pass_model"
        scene_render_pass_identifier = render_pass_model["identifier"]
        render_pass_model["backgroundIdentifier"] = last_render_pass_texture_identifier
        render_pass_model[
            "textureOutputIdentifier"
        ] = self.scene_render_pass_texture_identifier
        render_pass_model["sceneNodeIdentifier"] = self.device_identifier
        render_passes["children"].append(render_pass_model)

        last_render_pass_texture_identifier = self.scene_render_pass_texture_identifier
        last_render_pass_identifier = scene_render_pass_identifier

        if post_processor is not None:
            for i, render_pass_target in enumerate(post_processor.render_pass_targets):
                post_processor_render_pass_texture_identifier = post_processor.render_pass_texture_identifiers[
                    i
                ]

                # Create (Post) Processor Render Pass
                render_pass_model = self.create_processor_render_pass(
                    render_pass_target,
                    "postProcessorRenderPass_{}",
                    post_processor_render_pass_texture_identifier,
                    post_processor.material_identifiers[i],
                )
                render_passes["children"].append(render_pass_model)

                last_render_pass_texture_identifier = (
                    post_processor_render_pass_texture_identifier
                )
                last_render_pass_identifier = render_pass_model["identifier"]

        self.final_render_pass_identifier = last_render_pass_identifier

    def migrate_processor_shaders(self, shaders):
        print("Migrating processor shaders")

        if "children" not in shaders:
            shaders["children"] = []

        for processor_type in ProcessorType.all():
            index = int(processor_type)
            processor = self.processors[index]
            if processor is None:
                continue

            processor_name = processor_type.pretty_name
            processor.shader_identifier = make_random_identifier("shader_model")

            shader_model = {}
            shader_model["identifier"] = processor.shader_identifier
            shader_model["modelName"] = "shader_model"
            shader_model["name"] = processor_name
            shader_model["children"] = []
            children = shader_model["children"]

            assert processor.vertex_shader_model is not None
            assert processor.fragment_shader_model is not None

            children.append(deepcopy(processor.vertex_shader_model))
            children.append(deepcopy(processor.fragment_shader_model))

            shader_config_model = {}
            shader_config_model["identifier"] = make_random_identifier(
                "shader_config_model"
            )
            shader_config_model["modelName"] = "shader_config_model"
            shader_config_model["name"] = processor_name + "ShaderConfig"
            shader_config_model["excludeDuringExport"] = True
            shader_config_model["isExternalAsset"] = True
            shader_config_model["assetLocator"] = self.get_shader_config_path(
                processor_type
            )

            children.append(shader_config_model)

            shaders["children"].append(shader_model)

    def process_script(self, input_script):
        script_path = input_script["assetLocator"]

        print('Found script: "{}"'.format(script_path))
        self.script_paths.append(script_path)

        return deepcopy(input_script)

    def process_layers(self, input_layers):
        print("Processing layers")

        self.layer_identifier = input_layers["children"][0]["identifier"]

        return deepcopy(input_layers)

    def process_scene(self, input_scene):
        print("Processing scene")

        # Find first camera and device scene nodes
        S = [input_scene]

        while S:
            item = S[-1]

            if item["modelName"] == "device":
                self.device_identifier = item["identifier"]

            if item["modelName"] == "camera":
                self.camera_identifier = item["identifier"]

            if "children" in item:
                S = item["children"] + S

            S = S[:-1]

        return deepcopy(input_scene)

    def process_scripts(self, input_scripts):
        print("Processing scripts")

        if "children" in input_scripts:
            for item in input_scripts["children"]:
                self.process_script(item)

        return deepcopy(input_scripts)

    def migrate_document_children(self, input_array):
        output_array = []
        materials = None
        render_passes = None
        scene = None
        shaders = None
        textures = None

        for item in input_array:
            if "modelName" in item:
                model = item["modelName"]
                if model == "layers":
                    item = self.process_layers(item)
                elif model == "materials":
                    materials = self.migrate_materials(item)
                    item = None
                elif model == "render_passes_model":
                    render_passes = self.migrate_render_passes(item)
                    item = None
                elif model == "shaders":
                    shaders = self.migrate_shaders(item)
                    item = None
                elif model == "scene":
                    scene = self.process_scene(item)
                    item = None
                elif model == "scripts_model":
                    item = self.process_scripts(item)
                elif model == "textures":
                    textures = self.migrate_textures(item)
                    item = None
                elif model == "manifest":
                    item = self.migrate_manifest(item)
                elif model == "preprocessor":
                    item = self.migrate_processor(item, ProcessorType.PRE)
                elif model == "postprocessor":
                    item = self.migrate_processor(item, ProcessorType.POST)

            if item is not None:
                output_array.append(item)

        # Add render passes (old project)
        if render_passes is None:
            render_passes = {}
            model_name = "render_passes_model"
            render_passes["identifier"] = make_random_identifier(model_name)
            render_passes["modelName"] = model_name

        assert materials is not None
        assert shaders is not None
        assert textures is not None

        for processor_type in ProcessorType.all():
            index = int(processor_type)
            processor = self.processors[index]
            if processor is None:
                continue

            for j, texture_identifier in enumerate(processor.texture_identifiers):
                if "children" in textures:
                    for texture in textures["children"]:
                        if texture_identifier == texture["identifier"]:
                            processor.texture_names[j] = texture["name"]
                            break

        # Migrate shaders first to generate identifier required to create materials
        self.migrate_processor_shaders(shaders)
        self.create_null_object(scene)
        self.create_processor_render_pass_textures(textures)
        self.create_processor_materials(materials)
        self.create_processor_render_passes(render_passes, textures)

        output_array.append(materials)
        output_array.append(render_passes)
        output_array.append(scene)
        output_array.append(shaders)
        output_array.append(textures)

        return output_array

    # Find version and reject anything not supported by Skylight
    def check_project_version(self, input_dict):
        project_version = input_dict.get("version", {}).get("prod", 0)
        if project_version < MIN_PROJECT_VERSION:
            msg = (
                "Project version v{} is older than v49: "
                "please upgrade first using a recent version of AR Studio (Classic)"
            )
            print(msg.format(project_version, file=sys.stderr))
            sys.exit(1)

    # Check whether Custom Pre-/Post-Processors are actually used
    def check_project_processors(self, input_dict):
        children = input_dict.get("children", [])
        manifests = (
            child
            for child in children
            if "modelName" in child and child["modelName"] == "manifest"
        )
        if not manifests:
            return
        manifest = next(manifests)
        has_pre_processor = manifest.get("shaderPreProcessor", False)
        has_post_processor = manifest.get("shaderPostProcessor", False)
        if has_pre_processor or has_post_processor:
            return

        msg = (
            "Project is not using Custom Pre-/Post-Processors: "
            "please open it directly using a recent version of AR Studio (Skylight)"
        )
        print(msg, file=sys.stderr)
        sys.exit(0)

    def migrate_document(self, input_dict):
        self.check_project_version(input_dict)
        self.check_project_processors(input_dict)

        output_dict = {}

        for key, value in input_dict.items():
            if key == "children":
                value = self.migrate_document_children(value)

            output_dict[key] = value

        output_dict["finalRenderPassIdentifier"] = self.final_render_pass_identifier

        return output_dict

    def migrate_arproj(self, input_data):

        # Input and output arproj binary data
        with BytesIO(input_data) as input_b:
            with BytesIO() as output_b:
                # Input and output arproj zip archives
                with ZipFile(input_b, "r") as input_z:
                    with ZipFile(output_b, "w", **ZIP_FILE_KWARGS) as output_z:
                        for name in input_z.namelist():
                            # Input and output arproj zip entries
                            with input_z.open(name, "r") as input_f:
                                input_data = input_f.read()

                                if name != "main.json":
                                    output_z.writestr(name, input_data)
                                    continue

                                input_str = input_data.decode("UTF-8")
                                document_dict = json.loads(input_str)
                                document_dict = self.migrate_document(document_dict)
                                output_str = json.dumps(
                                    document_dict, sort_keys=True, indent=2
                                )
                                output_data = output_str.encode(encoding="UTF-8")
                                output_z.writestr(name, output_data)

                return output_b.getvalue()


def migrate_arprojpkg(input_path, output_path):
    memory_f = io.BytesIO()

    # Input and output arproj zip archives
    with ZipFile(input_path, "r") as input_z:
        with ZipFile(memory_f, "w", **ZIP_FILE_KWARGS) as output_z:
            # Locate the .arproj file to:
            # - Migrate it first, then
            # - Migrate shaders and script based on the processed data
            names = input_z.namelist()
            arproj_name = None
            for name in names:
                _, ext = os.path.splitext(name)
                if ext == ".arproj":
                    if arproj_name is not None:
                        raise RuntimeError("Multiple .arproj found inside project")
                    arproj_name = name

            if arproj_name is None:
                raise RuntimeError("No .arproj found inside project")

            migrator = ProjectMigrator()
            processed_names = []

            # Migrate .arproj file
            with input_z.open(arproj_name, "r") as input_f:
                input_data = input_f.read()
                output_data = migrator.migrate_arproj(input_data)
            output_z.writestr(arproj_name, output_data)
            processed_names.append(arproj_name)

            # Migrate processor shaders
            for shader_name in migrator.shader_paths:
                if shader_name.startswith("ColorLutPreset"):
                    if shader_name.endswith(".vsh"):
                        input_data = COLOR_LUT_PRESET_VERTEX_SHADER
                    if shader_name.endswith(".fsh"):
                        input_data = COLOR_LUT_PRESET_FRAGMENT_SHADER
                else:
                    with input_z.open(shader_name, "r") as input_f:
                        input_data = input_f.read()

                output_data = migrator.migrate_shader(input_data, shader_name)
                output_z.writestr(shader_name, output_data)
                processed_names.append(shader_name)

            # Generate processor shader config
            for processor_type in ProcessorType.all():
                config_name = migrator.get_shader_config_path(processor_type)

                if config_name is None:
                    continue

                output_data = migrator.migrate_shader_config(processor_type)
                output_z.writestr(config_name, output_data)

            # Migrate scripts
            for script_name in migrator.script_paths:
                with input_z.open(script_name, "r") as input_f:
                    input_data = input_f.read()
                    output_data = migrator.migrate_script(input_data, script_name)
                output_z.writestr(script_name, output_data)
                processed_names.append(script_name)

            for name in names:
                if name in processed_names:
                    continue

                with input_z.open(name, "r") as input_f:
                    input_data = input_f.read()
                output_z.writestr(name, input_data)

    with open(output_path, "w") as output_f:
        memory_f.seek(0)
        output_data = memory_f.read()
        output_f.write(output_data)


def make_parser():
    parser = argparse.ArgumentParser(
        description="Migrate a Classic project using Pre-/Post-Processors."
    )
    parser.add_argument(
        "-i",
        "--input-project",
        metavar="INPUT_PROJECT",
        action="store",
        type=str,
        help="path to a project to migrate",
    )
    parser.add_argument(
        "-o",
        "--output-project",
        metavar="OUTPUT_PROJECT",
        action="store",
        type=str,
        help="path to the migrated project",
    )
    return parser


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = make_parser()
    args = parser.parse_args(argv)
    if args.input_project is None or args.output_project is None:
        parser.print_help()
        return 1

    print('Input project: "{}"'.format(args.input_project))

    migrate_arprojpkg(args.input_project, args.output_project)

    print('Output project: "{}"'.format(args.output_project))
    return 0


if __name__ == "__main__":
    sys.exit(main())
