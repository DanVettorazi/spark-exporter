#!/usr/bin/python
import sys, getopt
import os
from glob import glob
import time
import subprocess

chapters = []
SparkExporter = '/Applications/Spark\ AR\ Studio_v82.app/Contents/MacOS/sparkTerminalAppleMac'

filenames = []
versions = []

def CreateList():
	
	chapters = glob(os.path.join(inputfolder,'*'))
	for chapter in chapters:
		filename = chapter.split('/')
		filename = str(filename[-1])
		#chapterVersion = versions[filenames.index(filename)]
		#renametag= '_v'+str(chapterVersion)+'_ARS82_en_US'
		chapterfile = os.path.join(chapter, filename+'.arproj') 
		pkgFolderName = 'pkgs'
		pkgFolder = os.path.join(outputfolder, pkgFolderName)
		# os.mkdir(pkgFolder)
		resultPkg = subprocess.check_output('sudo '+SparkExporter+' package '+chapterfile+' -d '+ pkgFolder, shell=True)
		packagedFile = os.path.join(outputfolder, pkgFolderName,filename+'.arprojpkg')
		resultExp = subprocess.check_output(SparkExporter+' export '+ packagedFile +' -d '+ outputfolder, shell=True)
		print('------------------')


def main(argv):
	#checkVersion()
	#print(str(sys.argv))
	global inputfolder
	global outputfolder
	global mode
	global version
	outputfolder = ''
	version = 1
	try:
		opts, args = getopt.getopt(argv, "echi:o:", ["export","convert","input=", "output="])
	except getopt.GetoptError:
	  print ('Command not accepted | StorytimeExporter.py -i <inputfolder> -o <outputfolder>')
	  sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print ('HELP: Storytime-Exporter.py -i <inputfolder> -o <outputfolder>')
			sys.exit()
		elif opt in ("-i", "--input"):
			inputfolder = arg
		elif opt in ("-o", "--output"):
			outputfolder = arg
		elif opt in("-e", "--export"):
			mode = 'export'
		elif opt in("-c", "--convert"):
			mode = 'convert'
	if outputfolder=='':
		outputfolder = os.getcwd()

	print ('Chapters folder is:', inputfolder)
	print ('Build folder is:', outputfolder)
	print ('initianting: ', mode)
	CreateList()

if __name__ == "__main__":
   main(sys.argv[1:])
