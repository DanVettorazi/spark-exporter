<h1 align="center"><strong> Spark Converter, Exporter and Packer</strong></h1>

<br />

<div align="center"><img src="imgs/logo.jpg" /></div>

<div align="center"><strong>📦 Python application to export or package Spark Projects in Batch 🎁</strong></div>

## Features

- **No Spark needed:** Need to Export or package a project to a especic version of Spark, but you don't have that version? No problem! All files and Spark versions come pre-assembled in this tool.
- **Export dozens of Spark Projects in seconds:** This exporter doesn't open any Spark UI, and also uses sub coroutines, this makes this tool be able to run several exports in seconds.
- **Only Package:** If you just need to Package several projects to a certain version of Spark. You can, in one click!
- **Export and Package Projects**: Export and Package with one click.
- **Choose various Spark Versions:** This tool can export the Spark project to any supperior version of what Spark version was created.
- **No configuration overhead**: No need to install, the application uses Python 2.7 that's installed in all Macs.
- **Conversion of Old Spark Projects**: You can do an automated conversion of a PRE-SKYLIGHT version of Spark to a brand new version. Some errors can occur, but all are pretty easy to fix it.

<div align="center"><img src="imgs/top.gif" /></div>

## Requirements

No need to have Spark installed in the machine.
Although, this Application just runs In OSX systems because of the way Spark organzie folders and uses the command line to export projects.

<div align="center"><img src="imgs/versionspark.png" /></div>
You need to have Python 2.7 or 3 installed in the machine. (note: All macs come with Python pre-installed.)

To run the exporter and packager you just need to run and point where the files are.
To Run correctly the converter, you need to paste your file inside of the folder: `converter/all_effects/Name_of_your_book/`

## How to Use:

How to Install:
```
# 1. Clone:
git clone git@gitlab.com:DanVettorazi/spark-exporter.git

# 1. Run it:
cd spark-exporter && python cli-exporter.py -h

```

How to Export a batch of projects:
```
# 1. in the command line, type:
python cli-exporter.py  --export -v <VERSION OF SPARK THAT YOU WANT TO EXPORT> -i <THE PATH WHERE YOUR PROJECTS ARE> -o <PATH TO WHERE YOU WANT TO SAVE YOUR EXPORTED FILES.>
```

How to package a batch of Projects:
```
# 1. in the command line, type:
python cli-exporter.py  --package -i -v <THE PATH WHERE YOUR PROJECTS ARE> -o <PATH TO WHERE YOU WANT TO SAVE YOUR PACKAGED FILES.>

```

How to see which versions are possible to export or package your files:
```
# 1. in the command line, type:
python cli-exporter.py  -v -h

#this will show all versions possible to export your project.

```
How to convert a old version of Spark (Pre-Skylight), to Skylight:
```
# 1. in the command line, type:
python cli-exporter.py  --convert -v <Desired version to convert> -i <PATH OF THE FILE> -o <OUTPUT FOLDER>

#Important Note: Check the folder structure for this works

```

TODO:
-show structure of folder.

## Documentation

### Commands

* `export` - export a batch of files in the designed folder.
* `package` - Package a batch of files in the designed folder.
* `convert` - convert a batch of files to the OUTPUT folder.
* `-v, --version` - Define which version of Spark You will export/package your files.
* `-i, --input` - Input Folder.
* `-o, --output` - Ouput Folder.
* `-h, --help` - Help. This command also works in sub commands, such as export, convert, --version.

## Contributing
You can add your own spark versions on this project. More details and how to do that will be added here.

## TO DO
```
- add a importer of Spark versions.
- add all versions of Spark.
- customize output folders of the converter.
```
## Author

Daniel Vettorazi
